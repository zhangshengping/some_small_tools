
from PyQt5 import QtCore, QtGui, QtWidgets 
from main import Ui_MainWindow
from PyQt5.QtGui import QRegExpValidator
from PyQt5.QtCore import QRegExp 
from binascii import *
import re

class MainUi(QtWidgets.QMainWindow, Ui_MainWindow):

    def __init__(self, parent=None):
        super(MainUi, self).__init__(parent)
        # 设置数据格式校验器
        rx_dec = QRegExp("[0-9 ]+")
        validator_dec = QRegExpValidator(rx_dec)

        rx_hex = QRegExp("[0-9A-Fa-f ]+")
        validator_hex = QRegExpValidator(rx_hex)

        rx_oct = QRegExp("[0-7 ]+")
        validator_oct = QRegExpValidator(rx_oct)

        rx_bin = QRegExp("[01 ]+")
        validator_bin = QRegExpValidator(rx_bin)

        self.setupUi(self)

        # 设置数据格式校验器
        self.decEdit.setValidator(validator_dec)
        self.hexEdit.setValidator(validator_hex)
        self.octEdit.setValidator(validator_oct)
        self.binEdit.setValidator(validator_bin)
        
        # 设置回调函数
        # self.decEdit.textChanged.connect(self.dec_convert)
        self.decEdit.textChanged.connect(self.dec_convert)

    def dec_convert(self, text):
        try:
            text = re.sub(" +$", "", text)
            text = re.sub("  +", " ", text)
            if(not text):
                return
            print(text)
            words = text.split(" ")
            print(words)
            values = [int(s) for s in words]
            hex_text = " ".join((hex(value)[2:] for value in values))
            bin_text = " ".join((bin(value)[2:] for value in values))
            oct_text = " ".join((oct(value)[2:] for value in values))
            self.hexEdit.setText(hex_text)
            self.binEdit.setText(bin_text)
            self.octEdit.setText(oct_text)
        except:
            print("error in function dec_convert")

if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    ui = MainUi()
    ui.show()
    sys.exit(app.exec_())